module.exports = {
  events: require('./events'),
  actions: require('./actions'),
  package: require('./package.json'),
}
