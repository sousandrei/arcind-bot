const { join } = require('path')
const { readFileSync } = require('fs')

exports.baseMsg = readFileSync(join(__dirname, './baseMsg.txt'))

exports.mappings = [
  { emoji: '🤔', role: 'Personnel-Staff' },
  { emoji: '😄', role: 'Recruitment Mgr' },
]
