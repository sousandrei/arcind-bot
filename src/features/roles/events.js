const {
  DISCORD: { GUILD },
  ROLES: { CHANNEL },
} = global.config

const { handleMessageReactionEvents, checkMessage } = require('./actions')

module.exports = async (bot) => {
  const guild = bot.client.guilds.cache.find(({ name }) => name == GUILD)
  const channel = guild.channels.cache.find(({ name }) => name == CHANNEL)

  const message = await checkMessage(channel)

  handleMessageReactionEvents(bot, guild, message)
}
