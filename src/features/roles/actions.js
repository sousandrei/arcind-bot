const logger = global.logger

const { baseMsg, mappings } = require('./mappings')

const validateReaction = async (botId, message, reaction, user) => {
  if (user.id == botId || message != reaction.message.id) {
    throw Error('wrong message')
  }

  // When we receive a reaction we check if the reaction is partial or not
  if (reaction.partial) {
    // If the message this reaction belongs to was removed the fetching might result in an API error, which we need to handle
    try {
      await reaction.fetch()
    } catch (error) {
      logger.error('Something went wrong when fetching the message: ', error)
      // Return as `reaction.message.author` may be undefined/null
      throw Error('wrong message')
    }
  }
}

const handleReactionEvent = async (
  botId,
  roles,
  guild,
  message,
  reaction,
  user,
  action,
) => {
  try {
    await validateReaction(botId, message, reaction, user)
  } catch (error) {
    return
  }

  const emoji = reaction.emoji.name
  const roleMapping = mappings.find((role) => role.emoji == emoji)

  if (!roleMapping) {
    await reaction.remove()
    return
  }

  const member = await guild.members.fetch(user.id)
  const role = roles.find((role) => role.name == roleMapping.role)

  try {
    if (action == 'ADD') {
      logger.debug('add role: ', role.name)
      await member.roles.add(role)
    } else {
      logger.debug('remove role: ', role.name)
      await member.roles.remove(role)
    }
  } catch (error) {
    error(error)
  }
}

exports.handleMessageReactionEvents = async (bot, guild, message) => {
  const botId = bot.client.user.id
  const roles = guild.roles.cache

  bot.client.on('messageReactionAdd', async (reaction, user) =>
    handleReactionEvent(botId, roles, guild, message, reaction, user, 'ADD'),
  )

  bot.client.on('messageReactionRemove', async (reaction, user) =>
    handleReactionEvent(botId, roles, guild, message, reaction, user, 'DEL'),
  )
}

exports.checkMessage = async (channel) => {
  const messages = await channel.messages.fetch({ limit: 10 })

  for (let [, message] of messages) {
    await message.delete()
  }

  let msg = baseMsg

  for (let map of mappings) {
    msg += `\n${map.emoji}: ${map.role}`
  }

  const message = await channel.send(msg)

  for (let map of mappings) {
    message.react(map.emoji)
  }

  return message
}
