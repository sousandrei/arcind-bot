const axios = require('axios')
// need to change
const { stringify } = require('qs')

const logger = global.logger
const { SERVER_URL, API_TOKEN } = global.config

exports.handleRegister = async (message) => {
  const { author, content } = message

  if (content != '!register') {
    return
  }

  let response

  try {
    response = await axios.post(
      SERVER_URL,
      stringify({
        token: API_TOKEN,
        query: 'register',
        data: {
          id: author.id,
          user: author.username,
        },
      }),
    )
    response = response.data
  } catch (error) {
    logger.error(error)
    return message.reply('An unknown error occurred.')
  }

  if (response.Error) {
    return message.reply(response.Error)
  }

  if (!response.Success) {
    return message.reply('An unknown error occurred.')
  }

  await author.send(response.Success, { split: true })

  if (message.channel.type !== 'dm') {
    message.reply("I've sent you a DM with " + 'the registration instructions!')
  }
}
