module.exports = {
  events: require('./events'),
  routes: require('./routes'),
  actions: require('./actions'),
  package: require('./package.json'),
}
