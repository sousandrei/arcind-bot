exports.handleTestRoute = function (req, res) {
  if (!req.body.message) {
    return res.status(400).end()
  }

  return res.end()
}

exports.handlePing = async ({ channel, content }) => {
  if (content != '!ping') {
    return
  }

  await channel.send('!pong')
}
