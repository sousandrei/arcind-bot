const { handleTestRoute } = require('./actions')

module.exports = (app) => {
  app.route('/geto').post((req, res) => handleTestRoute(req, res))
}
