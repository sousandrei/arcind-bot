const { YOUTUBE: CALLBACK_URL } = global.config

const pubSubHubbub = require('pubsubhubbub')
const { handlePubSub } = require('./actions')

module.exports = (app) => {
  const pubSub = pubSubHubbub.createServer({
    callbackUrl: CALLBACK_URL,
  })

  pubSub.on('feed', handlePubSub)

  app.use('/webhook', pubSub.listener())
}
