const { handleNewVideo } = require('./actions')

module.exports = (bot) => {
  const { enable } = require('./package.json')

  if (!enable) {
    return
  }

  process.on('newVideo', (link) => handleNewVideo(link, bot))
}
