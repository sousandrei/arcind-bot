module.exports = {
  routes: require('./routes'),
  events: require('./events'),
  actions: require('./actions'),
  package: require('./package.json'),
}
