const {
  DISCORD: { GUILD },
  YOUTUBE: { VIDEO_UPDATE_CHANNEL },
} = global.config

const logger = global.logger

const parseString = require('xml2js').parseString
const { get } = require('lodash')
const { promisify } = require('util')

const parseSync = promisify(parseString)

const channel = (bot) =>
  bot.client.guilds
    .find(({ name }) => name == GUILD)
    .channels.find(({ name }) => name == VIDEO_UPDATE_CHANNEL)

exports.handleNewVideo = async (link, bot) => {
  const videoChannel = channel(bot)
  await videoChannel.send(
    `Hey! **Star Citizen** just posted a video! Go check it out! \n${link}`,
  )
}

let dumbVideoMemory = []
exports.handlePubSub = async (data) => {
  logger.info('YouTube event incoming \n%o', data)

  let dataParsed = Buffer.from(data.feed).toString()

  try {
    dataParsed = await parseSync(dataParsed)
  } catch (error) {
    return logger.error('Error parsing YouTube data \n%o', error)
  }

  const videoId = get(dataParsed, ['feed', 'entry', '0', 'yt:videoId', '0'])

  if (dumbVideoMemory.includes(videoId)) {
    return logger.info('Video already processed \n%o', {
      videoId,
      dumbVideoMemory,
    })
  }

  dumbVideoMemory.push(videoId)

  const publishedDate = get(dataParsed, [
    'feed',
    'entry',
    '0',
    'published',
    '0',
  ])
  const link = get(dataParsed, ['feed', 'entry', '0', 'link', '0', '$', 'href'])

  const dateDiff = new Date() - new Date(publishedDate)

  if (dateDiff / 1000 > 60 * 5 || !link) {
    return logger.info('Video not new \n%o', {
      videoId,
      dateDiff,
      publishedDate,
    })
  }

  logger.info('Sending video update \n%o', { link })
  process.emit('newVideo', link)
}
