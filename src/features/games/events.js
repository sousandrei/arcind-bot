const { handlePresenceUpdate } = require('./actions')

const logger = global.logger

module.exports = (bot) => {
  const { enable } = require('./package.json')

  if (!enable) {
    return
  }

  bot.client.on('presenceUpdate', async (oldEvent, event) => {
    try {
      await handlePresenceUpdate(oldEvent, event)
    } catch (error) {
      logger.error(error)
    }
  })
}
