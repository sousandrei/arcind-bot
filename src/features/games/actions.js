const {
  DISCORD: { GUILD },
} = global.config

const mongoose = require('mongoose')

exports.handleGetActivities = async function (req, res) {
  const Activity = mongoose.model('Activity')

  //TODO: add pagination to make multiple queries
  // .limit(100)
  // .skip(100)
  // while have results

  const activities = await Activity.find().populate(['user', 'game'])

  // 1D array with days
  const activitiesByDay = []

  const users = []
  const games = []

  for (let activity of activities) {
    const { user, game, startTime, stopTime } = activity

    // ==================================
    // check if we have this user cached
    // if not add it
    // also add to it's playtime
    let cachedUserIndex = users.findIndex(({ _id }) => String(_id) == user._id)

    if (cachedUserIndex < 0) {
      users.push(user.toJSON())
      cachedUserIndex = users.length - 1
      users[cachedUserIndex].playTime = 0
    }

    users[cachedUserIndex].playTime += stopTime - startTime
    // ==================================

    // ==================================
    // check if we have this game cached
    // if not add it
    let cachedGameIndex = games.findIndex(({ _id }) => String(_id) == game._id)

    if (cachedGameIndex < 0) {
      games.push(game.toJSON())
      cachedGameIndex = games.length - 1
      games[cachedGameIndex].playTime = 0
    }

    games[cachedGameIndex].playTime += stopTime - startTime
    // ==================================

    // rolling date is the date we iterate through adding 1h each time
    let rollingDate = new Date(startTime)

    rollingDate.setMinutes(0)
    rollingDate.setSeconds(0)
    rollingDate.setMilliseconds(0)

    do {
      const day = rollingDate.getDay()
      const hour = rollingDate.getHours()

      const dayOfMonth = rollingDate.getDate()
      const month = rollingDate.getMonth()
      const year = rollingDate.getFullYear()

      const date = `${dayOfMonth}/${month}/${year}`

      const rollingDatePlusOne = rollingDate.getTime() + 3600 * 1000

      let dateIndex = activitiesByDay.findIndex((act) => act.date == date)

      if (dateIndex < 0) {
        activitiesByDay.push({
          date,
          day,
          hours: new Array(24).fill(null).map(() => []),
        })

        dateIndex = activitiesByDay.length - 1
      }

      let prevUserIndex = activitiesByDay[dateIndex].hours[hour].findIndex(
        (data) => data.user == user._id && data.game == game._id,
      )

      let minutesPlayed = 0

      if (rollingDate <= startTime && rollingDatePlusOne >= stopTime) {
        minutesPlayed += stopTime.getMinutes() - startTime.getMinutes()
      } else if (rollingDate <= startTime && rollingDatePlusOne <= stopTime) {
        minutesPlayed += 60 - startTime.getMinutes()
      } else if (rollingDate >= startTime && rollingDatePlusOne >= stopTime) {
        minutesPlayed += stopTime.getMinutes()
      } else if (rollingDate >= startTime && rollingDatePlusOne <= stopTime) {
        minutesPlayed += 60
      }

      rollingDate.setTime(rollingDatePlusOne)

      if (!minutesPlayed) {
        continue
      }

      if (prevUserIndex < 0) {
        activitiesByDay[dateIndex].hours[hour].push({
          user: user._id,
          game: game._id,
          type: activity.type,
          minutesPlayed: 0,
        })

        prevUserIndex = activitiesByDay[dateIndex].hours[hour].length - 1
      }

      activitiesByDay[dateIndex].hours[hour][
        prevUserIndex
      ].minutesPlayed += minutesPlayed
    } while (rollingDate <= stopTime)
  }

  return res.json({ activitiesByDay, users, games })
}

const upsertUser = async (member) => {
  const User = mongoose.model('User')

  const { displayName } = member
  const { id, avatar, discriminator, username } = member.user

  return await User.findOneAndUpdate(
    { id },
    {
      id,
      displayName,
      avatar,
      discriminator,
      username,
    },
    { upsert: true, new: true },
  )
}

const upsertGame = async (activity) => {
  const Game = mongoose.model('Game')

  const { name, applicationID: applicationId } = activity

  return await Game.findOneAndUpdate(
    { name, applicationId },
    {
      name,
      applicationId,
    },
    { upsert: true, new: true },
  )
}

const upserActivity = async (user, game, activity) => {
  const Activity = mongoose.model('Activity')

  const { startTime, type } = activity

  return await Activity.create({
    user: user._id,
    game: game._id,
    startTime,
    type,
    stopTime: Date.now(),
  })
}

const saveActivity = async (user, activity) => {
  const game = await upsertGame(activity)
  return await upserActivity(user, game, activity)
}

//TODO: better way of keeping track?
const MEMORY = []

//TODO: fill with logs
exports.handlePresenceUpdate = async (_, event) => {
  const { guild, userID, activities } = event

  if (guild.name != GUILD) {
    return
  }

  const member = await guild.members.fetch(userID)

  let userInMemory = MEMORY.find((entry) =>
    entry.id != member.user.id ? false : true,
  )

  if (!userInMemory) {
    userInMemory = { id: member.id, activities: [] }
    MEMORY.push(userInMemory)
  }

  const user = await upsertUser(member)
  const userIndex = MEMORY.findIndex((user) => user.id == userInMemory.id)

  // compare memory to discord: check if something is removed
  for (let oldActivity of userInMemory.activities) {
    let activity = activities.find(
      (newActivity) =>
        newActivity.applicationID == oldActivity.applicationID &&
        newActivity.name == oldActivity.name,
    )

    if (activity) {
      continue
    }

    await saveActivity(user, oldActivity)

    MEMORY[userIndex].activities = userInMemory.activities.filter(
      (userActivity) =>
        userActivity.applicationID != oldActivity.applicationID &&
        userActivity.name != oldActivity.name,
    )
  }

  // compare discord to memory: check if something is new in discord
  for (let newActivity of activities) {
    let activity = userInMemory.activities.find(
      (userActivity) =>
        userActivity.applicationID == newActivity.applicationID &&
        userActivity.name == newActivity.name,
    )

    if (activity) {
      continue
    }

    MEMORY[userIndex].activities.push({
      name: newActivity.name,
      type: newActivity.type,
      applicationID: newActivity.applicationID,
      startTime: Date.now(),
    })
  }
}
