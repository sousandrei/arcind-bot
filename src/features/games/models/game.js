const mongoose = require('mongoose')

const gamesSchema = new mongoose.Schema({
  name: String,
  applicationId: String,
})

mongoose.model('Game', gamesSchema)
