const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  id: String,
  avatar: String,
  username: String,
  discriminator: String,
  displayName: String,
})

mongoose.model('User', userSchema)
