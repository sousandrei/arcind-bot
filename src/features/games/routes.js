const { handleGetActivities } = require('./actions')

const logger = global.logger

module.exports = (app) => {
  app.route('/activities').get(async (req, res) => {
    try {
      await handleGetActivities(req, res)
    } catch (error) {
      logger.error(error.message)
      return res.status(500).end(error.message)
    }
  })
}
