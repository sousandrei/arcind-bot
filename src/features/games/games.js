module.exports = {
  events: require('./events'),
  routes: require('./routes'),
  actions: require('./actions'),
  package: require('./package.json'),
  models: {
    activity: require('./models/activity'),
    game: require('./models/game'),
    user: require('./models/user'),
  },
}
