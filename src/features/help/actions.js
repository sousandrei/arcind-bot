const { join } = require('path')
const { readdirSync } = require('fs')
const { pick, map } = require('lodash')

exports.handleHelp = async (message) => {
  const { author, channel, content } = message

  if (content.slice(0, 5) != '!help') {
    return
  }

  let modules = readdirSync('./src/features')

  if (content.length > 6) {
    let command = content.slice(6, content.length)

    if (modules.indexOf(command) == -1) {
      return message.reply("that's not a valid command!")
    }

    let path = join(__dirname, '../', command)
    let module = require(path)

    await author.send(formatHelpText(module), { split: true })

    if (channel.type == 'dm') {
      return
    }

    return message.reply(
      "I've sent you a DM with the" +
        ` instructions to use the '${command}' command!`,
    )
  }

  modules = modules
    .map((module) => {
      let path = join(__dirname, '../', module)
      module = require(path)

      return !module.package.enable || !module.package.cli
        ? undefined
        : formatHelpText(module)
    })
    .filter((module) => !!module)

  await author.send(
    "Here's a list of all my commands:\n\n" + modules.join('\n\n'),
    { split: true },
  )

  if (channel.type != 'dm') {
    await message.reply("I've sent you a DM with all my commands!")
  }
}

const formatHelpText = ({ package: pkg }) =>
  [
    `**!${pkg.name}**`,
    `name: ${pkg.name}`,
    `description: ${pkg.description}`,
    ...map(pick(pkg.meta, ['usage']), (value, key) => `${key}: ${value}`),
  ]
    .filter((str) => !!str)
    .join('\n')
