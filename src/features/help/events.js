const { handleHelp } = require('./actions')

module.exports = (bot) => {
  const { enable } = require('./package.json')

  if (!enable) {
    return
  }

  bot.client.on('message', handleHelp)
}
