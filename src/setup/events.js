const { readdirSync } = require('fs')

module.exports.register = async (bot) => {
  const events = readdirSync('./src/events')

  for (let event of events) {
    require(`../events/${event}`)(bot)
  }

  const features = readdirSync('./src/features')

  for (let feature of features) {
    feature = require(`../features/${feature}`)
    if (feature.package.enable && feature.events) {
      feature.events(bot)
    }
  }
}
