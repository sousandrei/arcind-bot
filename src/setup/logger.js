const {
  LOG: { LEVEL, FORMAT },
  PROJECT_NAME,
} = global.config

const { format, createLogger, transports } = require('winston')

const { combine, timestamp, label, printf, splat } = format

const formats = {
  json: printf(JSON.stringify),
  text: printf(
    (info) =>
      `${info.timestamp} [${info.label}]` + ` ${info.level}: ${info.message}`,
  ),
}

const logger = createLogger({
  level: LEVEL || 'info',
  format: combine(
    splat(),
    label({ label: PROJECT_NAME }),
    timestamp(),
    formats[FORMAT],
  ),
  transports: [
    new transports.Console({
      stderrLevels: ['error', 'crit', 'alert', 'emerg'],
    }),
  ],
})

module.exports = logger
