const {
  MONGO: { DB_URL },
} = global.config

const { readdirSync } = require('fs')

const mongoose = require('mongoose')
mongoose.set('useFindAndModify', false)

const logger = global.logger

module.exports = class DiscordBot {
  async connect() {
    // caching all features to run model initialization
    for (let feature of readdirSync('./src/features')) {
      require(`../features/${feature}`)
    }

    try {
      this.mongo = await mongoose.connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
    } catch (error) {
      logger.error(error.message)
      throw error
    }
  }

  async disconnect() {
    try {
      await this.mongo.disconnect()
      logger.info('Mongo disconnected')
    } catch (error) {
      logger.error(error.message)
      throw error
    }
  }
}
