const {
  EXPRESS: { PORT },
  PROJECT_NAME,
} = global.config

const EXPRESS_PORT = process.env.PORT || PORT

const cors = require('cors')
const morgan = require('morgan')
const express = require('express')
const { promisify } = require('util')
const { readdirSync } = require('fs')
const bodyParser = require('body-parser')

const { info } = global.logger

module.exports = class HttpServer {
  async start() {
    this.app = express()
    this.app.use(require('compression')())

    this.app.set('trust proxy', true)

    this.app.use(bodyParser.urlencoded({ extended: true }))
    this.app.use(bodyParser.json())

    this.app.use(require('method-override')())
    this.app.use(
      morgan(
        `:date[iso] [${PROJECT_NAME}] http: :method :url :status :response-time ms`,
      ),
    )

    this.app.use(cors())

    const features = readdirSync('./src/features')

    for (let feature of features) {
      feature = require(`../features/${feature}`)
      if (feature.package.enable && feature.routes) {
        feature.routes(this.app)
      }
    }

    this.app.get('/', (req, res) => res.status(200).end('ok'))
    this.app.all('*', (req, res) => res.status(404).end(':c'))

    this.server = require('http').createServer(this.app)

    await promisify(this.server.listen.bind(this.server))(EXPRESS_PORT)
    info(`HTTP server online on port ${EXPRESS_PORT}`)
  }
  async stop() {
    info('exiting')

    await new Promise((resolve) => this.server.close(resolve))

    info('HTTP server offline')
  }
}
