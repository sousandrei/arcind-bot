const {
  DISCORD: { BOT_TOKEN },
} = global.config

const Discord = require('discord.js')

const logger = global.logger

module.exports = class DiscordBot {
  async connect() {
    this.client = new Discord.Client()
    this.client.on('debug', (message) => logger.debug(message))

    try {
      await this.client.login(BOT_TOKEN)
      logger.info(`Connected as ${this.client.user.tag}`)
    } catch (error) {
      logger.error(error.message)
      throw error
    }

    await new Promise((resolve) => this.client.on('ready', resolve))
  }

  async disconnect() {
    try {
      await this.client.destroy()
      logger.info(`Disconnected as ${this.client.user.tag}`)
    } catch (error) {
      logger.error(error.message)
      throw error
    }
  }
}
