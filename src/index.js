global.config = require('../config')
global.logger = require('./setup/logger')

const events = require('./setup/events')
const DiscordBot = require('./setup/bot')
const HttpServer = require('./setup/express')
const Mongo = require('./setup/mongo')

const { info, error } = global.logger

async function main() {
  try {
    const mongo = new Mongo()
    await mongo.connect()

    const bot = new DiscordBot()
    await bot.connect()

    await events.register(bot)

    const server = new HttpServer()
    await server.start()

    process.on('SIGTERM', () => shutdown(server, bot))
    process.on('SIGINT', () => shutdown(server, bot))

    return { server, bot }
  } catch (err) {
    error('Error initializing service: ', err)
  }
}

async function shutdown(server, bot) {
  info('exiting')

  await server.stop()
  info('HTTP server offline')

  await bot.disconnect()
  info('bot offline')

  process.exit(0)
}

module.exports = main()
