const logger = global.logger

module.exports = (bot) => {
  bot.client.on('message', handleMessage)
}

function handleMessage(message) {
  const {
    author: { username },
    content,
  } = message

  logger.debug(`${username}: ${content}`)
}
