const { merge } = require('lodash')
const configYaml = require('config-yaml')

const getConfig = (config) => configYaml(`./config/${config}.yaml`)

const defaultConfig = getConfig('default')
const envConfig = getConfig(process.env.ENV || 'development')
const baseConfig = merge(defaultConfig, envConfig)

let config = baseConfig

try {
  const credentials = configYaml(`/run/secrets/credentials.yaml`)
  config = merge(baseConfig, credentials)
} catch (error) {
  process.stderr.write('WARKING: no credentials found\n')
}

module.exports = config
