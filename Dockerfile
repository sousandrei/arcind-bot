FROM node:lts-alpine

RUN apk add python make g++ libc-dev linux-headers --no-cache

WORKDIR /arcind-bot
COPY . .

RUN yarn --production
RUN yarn cache clean

ENTRYPOINT ["yarn"]
CMD [ "start" ]