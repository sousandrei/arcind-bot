# Arc Industries Bot

## Usage

### Configuration

Fill the files in the config folder

#### Log Levels

```
error:   0
warn:    1
info:    2
verbose: 3
debug:   4
silly:   5
```

#### Log Format

```
json
text
```

### Installing dependencies

```javascript
yarn
yarn start
```

### Running

```javascript
yarn start
```
